import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { FormDataResolverService } from './shared/form-data-resolver.service';
import { InformationRecipeComponent } from './information-recipe/information-recipe.component';
import {NotFoundComponent} from "./not-found.component";
import {MenuIsEmptyComponent} from "./menu-is-empty.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'creat-new-recipe', component: FormComponent},
  {path: ':id/edit-recipe', component: FormComponent, resolve: {formData: FormDataResolverService}},
  {path: ':id/information-recipe', component: InformationRecipeComponent, resolve: {formData: FormDataResolverService}},
  {path: 'menu-is-empty', component: MenuIsEmptyComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
