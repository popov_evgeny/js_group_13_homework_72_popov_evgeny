import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecipesModel } from '../shared/recipes.model';

@Component({
  selector: 'app-information-recipe',
  templateUrl: './information-recipe.component.html',
  styleUrls: ['./information-recipe.component.css']
})
export class InformationRecipeComponent implements OnInit {
  recipe: RecipesModel | null = null;
  loading!: boolean;

  constructor(
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.route.data.subscribe(data => {
      setTimeout(() => {
        this.recipe = <RecipesModel | null>data.formData;
        this.loading = false;
      }, 500);
    });
  }
}

