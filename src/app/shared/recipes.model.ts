export class RecipesModel {
  constructor(
    public id: string,
    public image: string,
    public nameRecipes: string,
    public description: string,
    public ingredients: string,
    public steps: Steps[]
  ) {}
}

export class Steps {
  constructor(
    public imageStep: string,
    public descriptionStep: string
  ) {
  }
}
