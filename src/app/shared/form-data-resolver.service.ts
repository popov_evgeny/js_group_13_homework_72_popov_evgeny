import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { RecipesModel } from './recipes.model';
import { RecipesService } from './recipes.service';

@Injectable({
  providedIn: 'root'
})
export class FormDataResolverService implements Resolve<RecipesModel> {

  constructor(
    private recipesService: RecipesService,
    private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<RecipesModel> | Observable<never>{
    const id = <string>route.params['id'];
    return this.recipesService.fetchFormData(id).pipe(mergeMap(formData => {
      if (formData) {
        return of (formData);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
