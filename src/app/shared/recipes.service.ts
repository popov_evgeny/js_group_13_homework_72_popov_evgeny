import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RecipesModel } from './recipes.model';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()

export class RecipesService{
  private recipesArray: RecipesModel[] = [];
  changesRecipesArray = new Subject <RecipesModel[]>();
  fetchingARecipesArrayLoading = new Subject<boolean>();
  addRecipesLoading = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  fetchingRecipes() {
    this.fetchingARecipesArrayLoading.next(true);
    this.http.get<{[id: string]: RecipesModel}>('https://project-f65ad-default-rtdb.firebaseio.com/recipes.json').pipe(map( result => {
      if (result === null) {
        return [];
      }
      return Object.keys(result).map( id => {
        const recipes = result[id];
        return new RecipesModel(id, recipes.image, recipes.nameRecipes, recipes.description, recipes.ingredients, recipes.steps);
      })
    })).subscribe( recipes => {
      this.recipesArray = recipes;
      this.changesRecipesArray.next(this.recipesArray.slice());
      this.fetchingARecipesArrayLoading.next(false);
    }, () =>{
      this.fetchingARecipesArrayLoading.next(false);
    })
  }


  fetchFormData(id: string){
    return this.http.get<RecipesModel | null>(`https://project-f65ad-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(map(result => {
      if (!result) return null;
      return new RecipesModel(
        id, result.image, result.nameRecipes,
        result.description, result.ingredients,
        result.steps
      );
    }));
  }

  addFormData(formData: RecipesModel) {
    this.addRecipesLoading.next(true);
    const body = {
      image: formData.image,
      nameRecipes: formData.nameRecipes,
      description: formData.description,
      ingredients: formData.ingredients,
      steps: formData.steps
    };
    return this.http.post('https://project-f65ad-default-rtdb.firebaseio.com/recipes.json', body).pipe(tap(() => {
      this.addRecipesLoading.next(false);
    }, () => {
      this.addRecipesLoading.next(false);
    }));
  }

  editFormData(formData: RecipesModel) {
    this.addRecipesLoading.next(true);
    const body = {
      image: formData.image,
      nameRecipes: formData.nameRecipes,
      description: formData.description,
      ingredients: formData.ingredients,
      steps: formData.steps
    };
    return this.http.put(`https://project-f65ad-default-rtdb.firebaseio.com/recipes/${formData.id}.json`, body).pipe(tap(() => {
      this.addRecipesLoading.next(false);
    }, () => {
      this.addRecipesLoading.next(false);
    }));
  }

  onRemoveRecipeStep(idRecipe: string, indexStep: number) {
    return this.http.delete(`https://project-f65ad-default-rtdb.firebaseio.com/recipes/${idRecipe}/steps/${indexStep}.json`);
  }

  onRemoveRecipe(id: string) {
    return this.http.delete(`https://project-f65ad-default-rtdb.firebaseio.com/recipes/${id}.json`);
  }

}
