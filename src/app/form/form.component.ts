import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { RecipesService } from '../shared/recipes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipesModel, Steps } from '../shared/recipes.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {
  reactiveForm!: FormGroup;
  recipe!: RecipesModel;
  addLoading!: Subscription;
  isEdit = false;
  isOpen = true;
  loading = true;
  editedId = '';
  indexStep!: any;
  countArrayLength = 0;

  constructor(
    private recipesService: RecipesService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.countArrayLength = 0;
    this.indexStep = null;
    this.addLoading = this.recipesService.addRecipesLoading.subscribe(loading => {
      this.loading = loading;
    })
    this.reactiveForm = new FormGroup({
      image: new FormControl('', Validators.required),
      nameRecipes: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([])
    });
    this.route.data.subscribe(data => {
      const formData = <RecipesModel | null>data.formData;
      this.recipe = data.formData;

      if (formData) {
        this.isEdit = true;
        this.editedId = formData.id;
        this.setFormValue({
          image: formData.image,
          nameRecipes: formData.nameRecipes,
          description: formData.description,
          ingredients: formData.ingredients,
          steps: []
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          image: '',
          nameRecipes: '',
          description: '',
          ingredients: '',
          steps: []
        });
      }
    })
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.reactiveForm.setValue(value);
    });
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.reactiveForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  fieldSkillsError(fieldName: string, index: number, errorType: string) {
    const steps = <FormArray>this.reactiveForm.get('steps');
    const field = steps.controls[index].get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  creatSteps(data: Steps) {
    const steps = <FormArray>this.reactiveForm.get('steps');
    const step = new FormGroup({
      imageStep: new FormControl(`${data.imageStep}`, Validators.required),
      descriptionStep: new FormControl(`${data.descriptionStep}`, Validators.required),
    });
    steps.push(step);
  }

  addSteps() {
    if (this.isEdit) {
      if (this.recipe?.steps && this.recipe?.steps.length !== this.countArrayLength) {
        this.recipe?.steps.forEach((data: Steps) => {
          this.countArrayLength = <number>this.recipe?.steps.length;
          this.isOpen = false;
          this.creatSteps(data);
        });
      } else {
        this.isOpen = false;
        this.creatSteps({imageStep: '', descriptionStep: ''});
      }
    } else {
      this.isOpen = false;
      this.creatSteps({imageStep: '', descriptionStep: ''});
    }
  }

  getSteps() {
    return (<FormArray>this.reactiveForm.get('steps')).controls
  }

  onSubmit() {
    const id = this.editedId || Math.random().toString();
    const formData = new RecipesModel(
      id, this.reactiveForm.value.image,
      this.reactiveForm.value.nameRecipes,
      this.reactiveForm.value.description,
      this.reactiveForm.value.ingredients,
      this.reactiveForm.value.steps
    );
    const next = () => {
      this.recipesService.fetchingRecipes();
    }
    if (this.isEdit) {
      this.recipesService.editFormData(formData).subscribe(next);
      setTimeout(() => {
        void this.router.navigate(['/']);
      }, 1000);
    } else {
      this.recipesService.addFormData(formData).subscribe(next);
      setTimeout(() => {
        void this.router.navigate(['/']);
      }, 1000);
    }
  }

  onRemoveStep(indexStep: number) {
    if (this.isEdit) {
      this.indexStep = indexStep;
      this.recipesService.onRemoveRecipeStep(this.recipe.id, indexStep).subscribe(() => {
        void this.router.navigate([this.recipe.id, 'edit-recipe']);
        setTimeout(() => {
          this.ngOnInit();
        }, 300);
      });
    } else {
      this.ngOnInit();
    }
  }

  ngOnDestroy() {
    this.addLoading.unsubscribe();
  }
}
