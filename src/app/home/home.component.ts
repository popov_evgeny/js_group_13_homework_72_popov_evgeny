import { Component, OnDestroy, OnInit } from '@angular/core';
import { RecipesService } from '../shared/recipes.service';
import { RecipesModel } from '../shared/recipes.model';
import { Subscription } from 'rxjs';
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy{
  recipesArray: RecipesModel[] | null = null;
  changeArraySubscription!: Subscription;
  fetchingArraySubscription!: Subscription;
  loading!: boolean;

  constructor(
    private recipesService: RecipesService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.changeArraySubscription = this.recipesService.changesRecipesArray.subscribe( array => {
      if (array.length > 0) {
        this.recipesArray = array;
      } else {
        void this.router.navigate(['menu-is-empty']);
      }
    });
    this.fetchingArraySubscription = this.recipesService.fetchingARecipesArrayLoading.subscribe( loading => {
      this.loading = loading;
    });
    this.recipesService.fetchingRecipes();
  }

  ngOnDestroy() {
    this.changeArraySubscription.unsubscribe();
    this.fetchingArraySubscription.unsubscribe();
  }

  onRemove(id: string) {
    this.recipesService.onRemoveRecipe(id).subscribe(() => {
      this.recipesService.fetchingRecipes();
    });
  }
}
