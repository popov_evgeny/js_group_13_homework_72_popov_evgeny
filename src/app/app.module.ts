import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RecipesService } from './shared/recipes.service';
import { HttpClientModule } from '@angular/common/http';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InformationRecipeComponent } from './information-recipe/information-recipe.component';
import {NotFoundComponent} from "./not-found.component";
import {MenuIsEmptyComponent} from "./menu-is-empty.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    FooterComponent,
    FormComponent,
    InformationRecipeComponent,
    NotFoundComponent,
    MenuIsEmptyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [RecipesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
